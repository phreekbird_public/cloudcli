FROM phreekbird/core:latest
MAINTAINER phreekbird webmaster@phreekbird.net
# now that we pulled the latest version of "I cannot Linux"-Ubuntu
# lets update repolists, upgrade, dist-upgrade, and clean up all the things.
# this will be my base container to build everything else from.
#
# Set DEBIAN_FRONTEND to noninteractive mode to silence debconf issues.
# and make sure to install apt-utils before we do anything to shut the thing up!
# aint nobody got time for dat!
RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install apt-utils curl wget -y && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y && DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y && DEBIAN_FRONTEND=noninteractive apt-get -f install && DEBIAN_FRONTEND=noninteractive apt-get autoremove -y && DEBIAN_FRONTEND=noninteractive apt-get clean -y
# lets get pip
RUN DEBIAN_FRONTEND=noninteractive apt-get install python-pip -y && pip install --upgrade pip
# lets get aws cli
RUN pip install awscli --upgrade --user
# time for google cloud sdk
RUN echo "deb http://packages.cloud.google.com/apt cloud-sdk-xenial main" > /etc/apt/sources.list.d/google-cloud-sdk.list
RUN wget https://packages.cloud.google.com/apt/doc/apt-key.gpg && apt-key add apt-key.gpg
RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install google-cloud-sdk -y
CMD /bin/bash
